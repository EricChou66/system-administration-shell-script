#!/bin/sh

usage(){ echo "Usage: weather [-h] -l locations [-u unit] [-a | -c | -d day | -s]"; }

get_woeid(){ woeid=$(curl -s woeid.rosselliot.co.nz/lookup/$1 | grep -e data-woeid="[0-9]*" | sed -e 's/"><td>.*//1' -e 's/.*data-woeid="//1'); }


get_weather(){ data=$(curl -s "weather.yahooapis.com/forecastrss?w=${woeid}&u=${unit}"); }

get_pos(){ pos=$(echo $data | sed -e 's/.*<yweather:location city="//g' -e 's/" region=".*//g'); }

current_cond(){ 
        cur_text=$(echo $data | sed -e 's/.*<yweather:condition text="//g' -e 's/" code=".*//g'); 
        cur_temp=$(echo $data | sed -e 's/.*temp="//g' -e 's/" date=".*//g');
        echo "${pos}, ${cur_text}, ${cur_temp}°${unit}";
}

weather_forecast() {
        forecast=$(echo $data | sed -e 's/.*<\/description> <yweather:forecast day="//1' | sed -e 's/code="[0-9]*" \/> .*//1');
        forecast_date=$(echo $forecast | sed -e 's/.*date="//g' -e 's/" low=".*/ /g');
        forecast_data=$(echo $forecast | sed -e 's/" date=.*low="/ /g' -e 's/" high="/°unit ~ /g' -e 's/" text="/°unit /g' -e 's/"//g' | sed -e "s/unit/${unit}/g");
        echo "${forecast_date}""${forecast_data}";
        forecast=$(echo $forecast | sed -e 's/\//\\\//g');
        day=$((day-1));
        while [ "${day}" != "0" ]
        do
                next=$(echo $data | sed -e "s/.*$forecast//g");
                forecast=$(echo $next | sed -e 's/code="[0-9]*" \/> <yweather:forecast day="//1' | sed -e 's/code="[0-9]*" \/> .*//1');
                forecast_date=$(echo $forecast | sed -e 's/.*date="//g' -e 's/" low=".*/ /g');
                forecast_data=$(echo $forecast | sed -e 's/" date=.*low="/ /g' -e 's/" high="/°unit ~ /g' -e 's/" text="/°unit /g' -e 's/"//g' | sed -e "s/unit/${unit}/g");
                echo "${forecast_date}""${forecast_data}";
                forecast=$(echo $forecast | sed -e 's/\//\\\//g');
                day=$((day-1));
        done
}

sunrise() { 
        sun=$(echo $data | sed -e 's/.*<yweather:astronomy sunrise="/sunrise: /g' -e 's/" sunset="/, sunset: /g' -e 's/".*//g'); 
        echo "${sun}";
}

if [ -f .weather.conf ]; then
        loc=$(head -n 1 .weather.conf | sed -e 's/location=//g');
        unit=$(tail -n 1 .weather.conf | sed -e 's/unit=//g');
fi
while getopts ":h :l: :u: :c :a :d: :s " opt; do
        case $opt in
                h)
                        usage
                        exit 1;
                        ;;
                l)
                        loc=${OPTARG}
                        ;;
                u)
                        unit=${OPTARG}
                        ;;
                c)
                        cur=$(echo "set")
                        ;;
                a)
                        all=$(echo "set")
                        ;;
                d)
                        dayset=$(echo "set")
                        day=${OPTARG}
                        ;;
                s)
                        sunset=$(echo "set")
                        ;;
                \?)
                        echo "Invalid option: -$OPTARG" >&t2;
                        usage
                        exit 1
                        ;;
                *)
                        echo "WTF"
                        ;;
        esac
done
if [ -z "${loc}" ]; then
        echo "Must specify locations";
        usage;
        exit 1;
elif [ -z "${unit}" ] && [ -z "${sunset}" ]; then
        echo "Must specify type of information";
        usage;
        exit 1;
elif [ -n "${unit}" ] && [ "${unit}" != "f" ] && [ "${unit}" != "c" ]; then
        echo "Wrong type of information" 
        echo "-u f for Fahrenheit and -u c for Celsius"
        exit 1;
else
        while true;
        do
                location=$(echo $loc | sed -e 's/,.*//g';)
                loc_t=$(echo $loc | sed -e "s/${location},//g");
                loc=$(echo "${loc_t}");
                get_woeid $location;
                get_weather;
                get_pos;
                if [ -n "${all}" ]; then
                        day=$(echo "5")
                        current_cond;
                        weather_forecast;
                        sunrise;
                fi
                if [ -n "${cur}" ] && [ -z "${all}" ]; then
                        current_cond;
                fi
                if [ -n "${dayset}" ] && [ -z "${all}" ]; then
                        weather_forecast;
                fi
                if [ -n "${sunset}" ] && [ -z "${all}" ]; then
                        sunrise;
                fi
                if [ $location == $loc ]; then
                        break;
                fi
        done
fi
