#!/bin/sh

rand(){ ran=$(od -An -N2 -i /dev/random); }

initgame() {
        rand
        ran1=$(( ($ran % 2 + 1) * 2 ));
        rand
        ran2=$(( ($ran % 2 + 1) * 2 ));
        rand
        ranpl1=$(( $ran % 16 + 1 ));
        rand
        ranpl2=$(( $ran % 16 + 1 ));
        while [ $ranpl1 -eq $ranpl2 ]
        do
                rand
                ranpl2=$(( $ran % 16 + 1 ));
        done
                form=$(echo "${form}" | sed -e "s/ ${ranpl1}_/ ${ranpl1}_${ran1}?/1");
                form=$(echo "${form}" | sed -e "s/ ${ranpl2}_/ ${ranpl2}_${ran2}?/1");
}

addrandom() {
        form=$(echo "${form}" | sed -e "s/_0/_/g")
        rand
        ranadd=$(( ($ran % 2 + 1) * 2 ));
        ranpl=$(( $ran % 16 + 1 ));
        to_add=$(echo "${form}" | grep -e " ${ranpl}_" | sed -e "s/.* ${ranpl}_//g" -e "s/|.*//g");
        rannext=$ranpl
        while [ "${to_add}" != "?" ]
        do
                rannext=$(( ( $rannext + 1 ) % 16  ))
                to_add=$(echo "${form}" | grep -e " ${rannext}_" | sed -e "s/.* ${rannext}_//g" -e "s/|.*//g");
                #if [ $rannext -eq $ranpl ]; then
                #        gameover="lose"
                #        break
                #fi
        done
        #if [ "${gameover}" != "lose" ]; then 
                form=$(echo "${form}" | sed -e "s/ ${rannext}_/ ${rannext}_${ranadd}/1");
        #fi
}

checkwin() {
        gameover=""
        i=16
        while [ $i -ge 0 ]
        do
                check=$(echo "${form}" | grep -e " ${i}_" | sed -e "s/.* ${i}_//g" -e "s/|.*//g");
                if [ "${check}" == "64?" ]; then
                        gameover="win"
                        break
                fi
                i=$((i-1))
        done
}

cleanindex() {
        i=16
        tmpform=$(echo "${form}")
        while [ $i -ge 0 ]
        do
                tmpform=$(echo "${tmpform}" | sed -e "s/${i}_//1")
                i=$((i-1))
        done
        tmpform=$(echo "${tmpform}" | sed -e "s/ |/!|/g" -e "s/^/!/g")
        tmpform=$(echo "${tmpform}" | tr "!?" "\t")
        form=$(echo "${form}" | sed -e 's/!//g')
        #form=$(echo "${form}" | sed -e 's/?//g')
}

index() {
form='---------------------------------
| | | | |
| 1_| 2_| 3_| 4_|
| | | | |
---------------------------------
| | | | |
| 5_| 6_| 7_| 8_|
| | | | |
---------------------------------
| | | | |
| 9_| 10_| 11_| 12_|
| | | | |
---------------------------------
| | | | |
| 13_| 14_| 15_| 16_|
| | | | |
---------------------------------';
}

handlekey() {
        if [ "${key}" == "w" ]; then
                for i in 1 2 3 4
                do
                        k=$i
                        step=0
                        a=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        na=$a
                        k=$((k+4))
                        b=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nb=$b
                        k=$((k+4))
                        c=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nc=$c
                        k=$((k+4))
                        d=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nd=$d
                        if [ "${nd}" == "" ]; then
                                unset nd
                        fi
                        if [ "${nc}" == "" ]; then
                                nc="${nd}"
                                unset nd
                        fi
                        if [ "${nb}" == "" ]; then
                                nb="${nc}"
                                nc="${nd}"
                                unset nd
                        fi
                        if [ "${na}" == "" ]; then
                                na="${nb}"
                                nb="${nc}"
                                nc="${nd}"
                                unset nd
                        fi
                        if [ "${nb}" == "${na}" ]; then
                                na=$((na+nb))
                                nb="${nc}"
                                nc="${nd}"
                                unset nd
                                step=1
                        fi
                        if [ "${nc}" == "${nb}" ] && [ $step -eq 0 ]; then
                                nb=$((nb+nc))
                                nc="${nd}"
                                unset nd
                                step=1
                        fi
                        if [ "${nd}" == "${nc}" ] && [ $step -eq 0 ]; then
                                nc=$((nd+nc))
                                unset nd
                        fi
                        k=$i
                        form=$(echo "${form}" | sed -e "s/ ${k}_${a}/ ${k}_${na}?/g");
                        k=$((k+4))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${b}/ ${k}_${nb}?/g");
                        k=$((k+4))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${c}/ ${k}_${nc}?/g");
                        k=$((k+4))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${d}/ ${k}_${nd}?/g");
                done
        elif [ "${key}" == "a" ]; then
                for i in 1 5 9 13
                do
                        k=$i
                        step=0
                        a=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        na=$a
                        k=$((k+1))
                        b=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nb=$b
                        k=$((k+1))
                        c=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nc=$c
                        k=$((k+1))
                        d=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nd=$d
                        if [ "${nd}" == "" ]; then
                                unset nd
                        fi
                        if [ "${nc}" == "" ]; then
                                nc="${nd}"
                                unset nd
                        fi
                        if [ "${nb}" == "" ]; then
                                nb="${nc}"
                                nc="${nd}"
                                unset nd
                        fi
                        if [ "${na}" == "" ]; then
                                na="${nb}"
                                nb="${nc}"
                                nc="${nd}"
                                unset nd
                        fi
                        if [ "${nb}" == "${na}" ]; then
                                na=$((na+nb))
                                nb="${nc}"
                                nc="${nd}"
                                unset nd
                                step=1
                        fi
                        if [ "${nc}" == "${nb}" ] && [ $step -eq 0 ]; then
                                nb=$((nb+nc))
                                nc="${nd}"
                                unset nd
                                step=1
                        fi
                        if [ "${nd}" == "${nc}" ] && [ $step -eq 0 ]; then
                                nc=$((nd+nc))
                                unset nd
                        fi
                        k=$i
                        form=$(echo "${form}" | sed -e "s/ ${k}_${a}/ ${k}_${na}?/g");
                        k=$((k+1))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${b}/ ${k}_${nb}?/g");
                        k=$((k+1))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${c}/ ${k}_${nc}?/g");
                        k=$((k+1))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${d}/ ${k}_${nd}?/g");
                done
        elif [ "${key}" == "s" ]; then
                for i in 1 2 3 4
                do
                        k=$i
                        step=0
                        a=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        na=$a
                        k=$((k+4))
                        b=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nb=$b
                        k=$((k+4))
                        c=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nc=$c
                        k=$((k+4))
                        d=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nd=$d
                        if [ "${na}" == "" ]; then
                                unset na
                        fi
                        if [ "${nb}" == "" ]; then
                                nb="${na}"
                                unset na
                        fi
                        if [ "${nc}" == "" ]; then
                                nc="${nb}"
                                nb="${na}"
                                unset na
                        fi
                        if [ "${nd}" == "" ]; then
                                nd="${nc}"
                                nc="${nb}"
                                nb="${na}"
                                unset na
                        fi
                        if [ "${nd}" == "${nc}" ]; then
                                nd=$((nd+nc))
                                nc="${nb}"
                                nb="${na}"
                                unset na
                                step=1
                        fi
                        if [ "${nc}" == "${nb}" ] && [ $step -eq 0 ]; then
                                nc=$((nb+nc))
                                nb="${na}"
                                unset na
                                step=1
                        fi
                        if [ "${nb}" == "${na}" ] && [ $step -eq 0 ]; then
                                nb=$((nb+na))
                                unset na
                        fi
                        k=$i
                        form=$(echo "${form}" | sed -e "s/ ${k}_${a}/ ${k}_${na}?/g");
                        k=$((k+4))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${b}/ ${k}_${nb}?/g");
                        k=$((k+4))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${c}/ ${k}_${nc}?/g");
                        k=$((k+4))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${d}/ ${k}_${nd}?/g");
                done
        elif [ "${key}" == "d" ]; then
                for i in 1 5 9 13
                do
                        k=$i
                        step=0
                        a=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        na=$a
                        k=$((k+1))
                        b=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nb=$b
                        k=$((k+1))
                        c=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nc=$c
                        k=$((k+1))
                        d=$(echo "${form}" | grep -e " ${k}_" | sed -e "s/.* ${k}_//g" -e "s/|.*//g");
                        nd=$d
                        if [ "${na}" == "" ]; then
                                unset na
                        fi
                        if [ "${nb}" == "" ]; then
                                nb="${na}"
                                unset na
                        fi
                        if [ "${nc}" == "" ]; then
                                nc="${nb}"
                                nb="${na}"
                                unset na
                        fi
                        if [ "${nd}" == "" ]; then
                                nd="${nc}"
                                nc="${nb}"
                                nb="${na}"
                                unset na
                        fi
                        if [ "${nd}" == "${nc}" ]; then
                                nd=$((nd+nc))
                                nc="${nb}"
                                nb="${na}"
                                unset na
                                step=1
                        fi
                        if [ "${nc}" == "${nb}" ] && [ $step -eq 0 ]; then
                                nc=$((nb+nc))
                                nb="${na}"
                                unset na
                                step=1
                        fi
                        if [ "${nb}" == "${na}" ] && [ $step -eq 0 ]; then
                                nb=$((nb+na))
                                unset na
                        fi
                        k=$i
                        form=$(echo "${form}" | sed -e "s/ ${k}_${a}/ ${k}_${na}?/g");
                        k=$((k+1))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${b}/ ${k}_${nb}?/g");
                        k=$((k+1))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${c}/ ${k}_${nc}?/g");
                        k=$((k+1))
                        form=$(echo "${form}" | sed -e "s/ ${k}_${d}/ ${k}_${nd}?/g");
                done
        fi
        form=$(echo "${form}" | sed -e "s/_0/_/g")
}

play_game() {
        if [ "${resume}" != "yes" ] && [ "${load}" != "yes" ]; then
                index
                initgame
        elif [ "${resume}" == "yes" ] && [ "${form}" == "" ] && [ "${gameover}" != "win" ] && [ "${gameover}" != "lose" ]; then
                dialog --ok-button 'Ok, start new game' --no-collapse --msgbox "Can't resume or load game" 8 30
                index
                initgame
        elif [ "${load}" == "yes" ] && [ "${form}" == "" ] && [ "${gameover}" != "win" ] && [ "${gameover}" != "lose" ]; then
                dialog --ok-button 'Ok, start new game' --no-collapse --msgbox "Can't resume or load game" 8 30
                index
                initgame
        elif [ "${gameover}" == "win" ]; then
                dialog --ok-button 'Ok, start new game' --no-collapse --msgbox "Can't resume because you are already win!" 8 30
                index
                initgame
        #elif [ "${gameover}" != "lose" ]; then
        #        dialog --ok-button 'Ok, start new game' --no-collapse --msgbox "Can't resume because you are alreday lose!" 8 30
        #        index
        #        initgame
        fi
        cleanindex
        while :
        do
                dialog --no-collapse --infobox "${tmpform}" 30 60
                key=`stty -icanon; dd bs=1 count=1 2>./.keyinput`
                if [ "${key}" == "q" ]; then
                        break;
                elif [ "${key}" == "w" ] || [ "${key}" == "a" ] || [ "${key}" == "s" ] || [ "${key}" == "d" ]; then
                        form=$(echo "${form}" | sed -e 's/?//g')
                        prevform=$(echo "${form}" | sed -e "s/|/?|/g" | sed -e "s/^?//g" -e "s/| ?/| /g")
                        handlekey
                        checkwin
                        if [ "${prevform}" != "${form}" ]; then
                                addrandom
                        fi
                        cleanindex
                fi
                #if [ "${gameover}" == "lose" ]; then
                #        dialog --ok-button 'Return to Main' --no-collapse --msgbox "${lose_title}" 15 80
                #        break;
                if [ "${gameover}" == "win" ]; then
                        dialog --ok-button 'Return to Main' --no-collapse --msgbox "${win_title}" 15 80
                        break;
                fi
        done
}

save_game() {
                if [ "${form}" == "" ]; then
                        dialog --ok-button 'Return to Main' --msgbox "You are saving a game which haven't started!" 15 50
                        return
                fi
                if [ "${gameover}" == "win" ]; then
                        dialog --ok-button 'Return to Main' --msgbox "You can't save a game that is already win!" 15 50
                        return
                fi
                #if [ "${gameover}" == "lose" ]; then
                        #dialog --ok-button 'Return to Main' --msgbox "You can't save a game that is already lose!" 15 50
                        #return
                #fi
                if [ ! -d ./2048data ]; then
                        `mkdir 2048data`
                fi
                if [ -e ./2048data/sav1 ]; then
                        sav1=$(head -n 1 ./2048data/sav1)
                else
                        sav1="empty"
                fi
                if [ -e ./2048data/sav2 ]; then
                        sav2=$(head -n 1 ./2048data/sav2)
                else
                fi
                        sav2="empty"
                if [ -e ./2048data/sav3 ]; then
                        sav3=$(head -n 1 ./2048data/sav3)
                else
                fi
                        sav3="empty"
                if [ -e ./2048data/sav4 ]; then
                        sav4=$(head -n 1 ./2048data/sav4)
                else
                        sav4="empty"
                fi
                if [ -e ./2048data/sav5 ]; then
                        sav5=$(head -n 1 ./2048data/sav5)
                else
                        sav5="empty"
                fi 
                dialog  --title "Save data" --menu "You can save at most 5 data" 15 60 8 "1" "${sav1}" "2" "${sav2}" "3" "${sav3}" "4" "${sav4}" "5" "${sav5}" 2> $sav_select
                Sav_s=$(cat $sav_select)
                now=$(date "+%Y-%m-%d %H:%M:%S")
                echo -e "${now}\n${form}" > "./2048data/sav${Sav_s}"
                rm -f "$sav_select"
}

load_game() {
                while :
                do
                if [ -e ./2048data/sav1 ]; then
                        ld1=$(head -n 1 ./2048data/sav1)
                else
                        ld1="empty"
                fi
                if [ -e ./2048data/sav2 ]; then
                        ld2=$(head -n 1 ./2048data/sav2)
                else
                fi
                        ld2="empty"
                if [ -e ./2048data/sav3 ]; then
                        ld3=$(head -n 1 ./2048data/sav3)
                else
                fi
                        ld3="empty"
                if [ -e ./2048data/sav4 ]; then
                        ld4=$(head -n 1 ./2048data/sav4)
                else
                        ld4="empty"
                fi
                if [ -e ./2048data/sav5 ]; then
                        ld5=$(head -n 1 ./2048data/sav5)
                else
                        ld5="empty"
                fi 
                dialog  --title "Load data" --menu "You can select one from 5 data" 15 60 8 "1" "${ld1}" "2" "${ld2}" "3" "${ld3}" "4" "${ld4}" "5" "${ld5}" 2> $load_select
                if [ ${?} -eq 1 ]; then
                        rm -f "$load_select"
                        return
                fi
                Ld_s=$(cat $load_select)
                empty=""
                case $Ld_s in
                "1")
                        if [ "${ld1}" == "empty" ]; then
                                empty="true"
                        fi
                        ;;
                "2")
                        if [ "${ld2}" == "empty" ]; then
                                empty="true"
                        fi
                        ;;
                "3")
                        if [ "${ld3}" == "empty" ]; then
                                empty="true"
                        fi
                        ;;
                "4")
                        if [ "${ld4}" == "empty" ]; then
                                empty="true"
                        fi
                        ;;
                "5")
                        if [ "${ld5}" == "empty" ]; then
                                empty="true"
                        fi
                        ;;
                esac
                if [ "${empty}" == "true" ]; then
                        dialog --ok-button 'Choose another save data' --msgbox "You are loading a data which is empty? " 15 50
                        form=$(cat "./2048data/sav${Ld_s}" | tail -n +2)
                else
                        break
                fi
                done
                rm -f "$load_select"
                form=$(cat "./2048data/sav${Ld_s}" | tail -n +2)
                play_game
}

game_title='
  ________  ________  _____ ______   _______
 |\   ____\|\   __  \|\   _ \  _   \|\  ___ \
 \ \  \___|\ \  \|\  \ \  \\\__\ \  \ \   __/|
  \ \  \  __\ \   __  \ \  \\|__| \  \ \  \_|/__
   \ \  \|\  \ \  \ \  \ \  \    \ \  \ \  \_|\ \
    \ \_______\ \__\ \__\ \__\    \ \__\ \_______\
     \|_______|\|__|\|__|\|__|     \|__|\|_______|
       _______  ________  ___   ___  ________
      /  ___  \|\   __  \|\  \ |\  \|\   __  \
     /__/|_/  /\ \  \|\  \ \  \\_\  \ \  \|\  \
     |__|//  / /\ \  \\\  \ \______  \ \   __  \
         /  /_/__\ \  \\\  \|_____|\  \ \  \|\  \
        |\________\ \_______\     \ \__\ \_______\
         \|_______|\|_______|      \|__|\|_______|
';

win_title='
  _____                        __       __     __  _         
 / ___/__  ___  ___ ________ _/ /___ __/ /__ _/ /_(_)__  ___ 
/ /__/ _ \/ _ \/ _ `/ __/ _ `/ __/ // / / _ `/ __/ / _ \/ _ \
\___/\___/_//_/\_, /_/  \_,_/\__/\_,_/_/\_,_/\__/_/\___/_//_/
              /___/ 
';

lose_title='
_____.___.              .__                       
\__  |   | ____  __ __  |  |   ____  ______ ____  
 /   |   |/  _ \|  |  \ |  |  /  _ \/  ___// __ \ 
 \____   (  <_> )  |  / |  |_(  <_> )___ \\  ___/ 
 / ______|\____/|____/  |____/\____/____  >\___  >
 \/                                     \/     \/ 
';

menu_select="./.menu.$$";
sav_select="./.sav.$$";
load_select="./.load.$$";

dialog --ok-button 'Play 2048' --no-collapse --msgbox "${game_title}" 20 60

while :
        do
        dialog  --title "menu" --menu "Command Line 2048" 15 60 8 N "New game - start a new 2048 game" R "Resume - Resume previous game" L "Load - Load from previous game" S "Save - Save current game state" Q "Quit" 2> $menu_select
        Menu_s=$(cat $menu_select)
        rm -f "$menu_select"
        case $Menu_s in                
        "N")      
                load="no"
                resume="no"
                play_game
                ;;
        "R")
                resume="yes"
                play_game
                ;;
        "L")
                load="yes"
                load_game
                ;;
        "S")
                save_game
                ;;
        "Q")
                break
                ;;
        esac
        done
